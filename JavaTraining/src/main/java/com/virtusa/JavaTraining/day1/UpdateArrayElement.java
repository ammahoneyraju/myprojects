package com.virtusa.JavaTraining.day1;

import java.util.Arrays;

public class UpdateArrayElement {

    String[] cities = {"Delhi", "Pune", "Hyderabad"};

    //Assuming no duplicates in array
    public void updateCity(String city, String updateToCity)
    {
        for(int i=0;i< cities.length;i++){
            if(cities[i].equals(city)){
                cities[i] = updateToCity;
            }
        }
    }

    public static void main(String[] args) {
        UpdateArrayElement updateArrayElement = new UpdateArrayElement();
        System.out.println("Before: "+ Arrays.toString(updateArrayElement.cities));
        updateArrayElement.updateCity("Pune", "Bangalore");
        System.out.println("After: "+ Arrays.toString(updateArrayElement.cities));


    }

}
