package com.virtusa.JavaTraining.day1;

import java.util.Map;
import java.util.TreeMap;

public class ReverseOrderOfKeysInTreeMap {

    TreeMap<Integer, Integer> studentIdMarks = new TreeMap<>(Map.of(1234, 90, 1578, 75, 1654, 87));

    public static void main(String[] args) {
        ReverseOrderOfKeysInTreeMap reverseOrderOfKeysInTreeMap = new ReverseOrderOfKeysInTreeMap();
        System.out.println(reverseOrderOfKeysInTreeMap.studentIdMarks.descendingKeySet());
    }
}
