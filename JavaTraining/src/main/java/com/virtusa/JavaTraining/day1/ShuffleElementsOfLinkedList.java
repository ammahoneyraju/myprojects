package com.virtusa.JavaTraining.day1;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class ShuffleElementsOfLinkedList {

    LinkedList<String> cities = new LinkedList<>(Arrays.asList("Hyderabad", "Bangalore", "Pune"));

    public void shuffle(){
        Collections.shuffle(cities);
        System.out.println("Shuffled List:" + cities.toString());
    }


    public static void main(String[] args) {
        ShuffleElementsOfLinkedList shuffleElementsOfLinkedList = new ShuffleElementsOfLinkedList();
        System.out.println("Original List:" + shuffleElementsOfLinkedList.cities);
        shuffleElementsOfLinkedList.shuffle();
    }
}
