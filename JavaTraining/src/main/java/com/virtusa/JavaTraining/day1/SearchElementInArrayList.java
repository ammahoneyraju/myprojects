package com.virtusa.JavaTraining.day1;

import java.util.List;

public class SearchElementInArrayList {

    List<String> customers = List.of("Raju", "Sandeep", "Pavan");

    public  boolean searchCustomer(String customer){

        return customers.stream().filter(customerName -> customerName.equals(customer)).findAny().isPresent();
    }

    public static void main(String[] args)   {
        SearchElementInArrayList searchElementInArrayList = new SearchElementInArrayList();
        System.out.println(searchElementInArrayList.searchCustomer("Sravanthi"));
        System.out.println(searchElementInArrayList.searchCustomer("Sandeep"));
    }
}
