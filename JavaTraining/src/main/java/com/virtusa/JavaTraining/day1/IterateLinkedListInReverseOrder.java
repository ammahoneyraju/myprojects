package com.virtusa.JavaTraining.day1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class IterateLinkedListInReverseOrder {


    List<String> cities = new LinkedList<>(Arrays.asList("Hyderabad", "Pune", "Bangalore"));

    public  void iterateInReverseOrder(){
        var itr = cities.listIterator(cities.size());
        while (itr.hasPrevious()){
            System.out.println(itr.previous());
        }
    }


    public static void main(String[] args) {
        IterateLinkedListInReverseOrder iterateLinkedListInReverseOrder = new IterateLinkedListInReverseOrder();
        iterateLinkedListInReverseOrder.iterateInReverseOrder();
    }
}
