package com.virtusa.JavaTraining.day1;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MapWithKeysGreaterThanGivenKey {

    Map<Integer,Integer> studentIdMarks = new HashMap<>();

    public Map<Integer, Integer> getStudentIdMarksGreaterThan(int studentId){
        return studentIdMarks.entrySet().stream().filter(entry-> entry.getKey() > studentId).collect(Collectors.toMap(entry->entry.getKey(), entry->entry.getValue()));
    }

    public static void main(String[] args) {
        MapWithKeysGreaterThanGivenKey mapWithKeysGreaterThanGivenValue = new MapWithKeysGreaterThanGivenKey();
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(123, 90);
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(111, 94);
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(145, 65);
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(153, 97);
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(112, 75);
        mapWithKeysGreaterThanGivenValue.studentIdMarks.put(109, 93);
        var result = mapWithKeysGreaterThanGivenValue.getStudentIdMarksGreaterThan(130);
        System.out.println(Collections.singletonList(result));

    }
}
