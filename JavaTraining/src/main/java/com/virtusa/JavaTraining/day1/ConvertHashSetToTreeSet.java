package com.virtusa.JavaTraining.day1;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ConvertHashSetToTreeSet {

    Set<String> elementsInSet = Set.of("Welcome", "To", "India");

    public Set<String> convert(){
        return new TreeSet<>(elementsInSet);
    }

    public Set<String> convertUsingAddAll(){
        var treeSet = new TreeSet<String>();
        treeSet.addAll(elementsInSet);
        return treeSet;
    }

    public Set<String> convertUsingForEach(){
        var treeSet = new TreeSet<String>();
        for(var element: elementsInSet){
          treeSet.add(element);
        }
        return treeSet;
    }
    
    public static void main(String[] args) {
        ConvertHashSetToTreeSet convertHashSetToTreeSet = new ConvertHashSetToTreeSet();
        System.out.println(convertHashSetToTreeSet.convert());
        System.out.println(convertHashSetToTreeSet.convertUsingAddAll());
        System.out.println(convertHashSetToTreeSet.convertUsingForEach());

    }

}
