package com.virtusa.JavaTraining.day1;

import java.util.HashMap;
import java.util.Map;

public class CheckMapIsEmpty {

    static Map<String, String> map = new HashMap<>();

    public static void main(String[] args) {
        boolean checkMapIsEmpty = map==null || map.isEmpty()?true: false;
        System.out.println(checkMapIsEmpty);
    }
}
