package com.virtusa.JavaTraining.day1;

import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

public class ClosestElementInTreeSet {

    TreeSet<Integer>  elements = new TreeSet<Integer>(Set.of(100, 55, 99));

    public Optional<Integer> getClosestElement(int key){
        var result =  elements.stream().filter(element -> element < key).findFirst();
        return result;
    }

    public static void main(String[] args) {
        ClosestElementInTreeSet closestElementInTreeSet = new ClosestElementInTreeSet();
        System.out.println("Closest Element By Traversing: " + closestElementInTreeSet.getClosestElement(70).get());
        System.out.println("Closest Element Using Api:" + closestElementInTreeSet.elements.floor(60));
    }


}
