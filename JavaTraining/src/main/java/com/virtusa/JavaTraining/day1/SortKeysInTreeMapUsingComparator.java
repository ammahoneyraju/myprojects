package com.virtusa.JavaTraining.day1;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

class File
{
    String name;
    int fileId;

    File(String name, int fileId){
        this.name = name;
        this.fileId = fileId;
    }
}

public class SortKeysInTreeMapUsingComparator {

    TreeMap<File, Integer> books = new TreeMap<File, Integer>((File file1, File file2)->{
        return file2.fileId - file1.fileId;
    });

    public static void main(String[] args) {
        SortKeysInTreeMapUsingComparator sortKeysInTreeMapUsingComparator = new SortKeysInTreeMapUsingComparator();
        sortKeysInTreeMapUsingComparator.books.put(new File("HarryPotter", 456), 1);
        sortKeysInTreeMapUsingComparator.books.put(new File("CharlottesWeb", 789), 2);
        sortKeysInTreeMapUsingComparator.books.put(new File("WonderLand", 123), 3);
        System.out.println(Collections.singletonList(sortKeysInTreeMapUsingComparator.books));

    }


}
