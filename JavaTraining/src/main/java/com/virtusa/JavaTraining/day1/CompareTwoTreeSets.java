package com.virtusa.JavaTraining.day1;

import java.util.Set;
import java.util.TreeSet;


class Student implements Comparable<Student>{

    int id;

    Student(int id){
        this.id = id;
    }

    @Override
    public int compareTo(Student student) {
        return this.id - student.id;
    }
}
public class CompareTwoTreeSets {

    public static void main(String[] args) {

        // set comparison for primitive values
        TreeSet<String> set1= new TreeSet<>(Set.of("Hyderabad", "Bangalore", "Pune"));
        TreeSet<String> set2= new TreeSet<>(Set.of("Hyderabad", "Pune", "Bangalore"));
        System.out.println(set1.equals(set2));

        //set comparison for custom objects
        TreeSet<Student> studentSet1= new TreeSet<Student>(Set.of(new Student(123), new Student(456)));
        TreeSet<Student> studentSet2= new TreeSet<Student>(Set.of(new Student(456), new Student(123), new Student(789)));
        System.out.println(studentSet1.equals(studentSet2));

    }


}
