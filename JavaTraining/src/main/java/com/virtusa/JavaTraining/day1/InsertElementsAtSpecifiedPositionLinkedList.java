package com.virtusa.JavaTraining.day1;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class InsertElementsAtSpecifiedPositionLinkedList {

    List<String>  customers = new LinkedList<>(Arrays.asList("Sandeep", "Prudhvi", "Tom", "Pavan"));

    public void addElementsAfterAnIndex(int index, String ... newCustomers){
        if(index >=0 && index < customers.size())
        customers.addAll(index, Arrays.asList(newCustomers));

    }

    public static void main(String[] args) {
        InsertElementsAtSpecifiedPositionLinkedList insertElementsAtSpecifiedPositionLinkedList = new InsertElementsAtSpecifiedPositionLinkedList();
        insertElementsAtSpecifiedPositionLinkedList.addElementsAfterAnIndex(2, "raju", "kiran");
        System.out.println(insertElementsAtSpecifiedPositionLinkedList.customers);

    }
}
