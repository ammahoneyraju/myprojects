package com.virtusa.JavaTraining.day1;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

public class PriorityQueueToArray {

    PriorityQueue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());

    public static void main(String[] args) {
        PriorityQueueToArray priorityQueueToArray = new PriorityQueueToArray();
        for(int i=1;i<=10;i++)
         priorityQueueToArray.queue.add(i);
        System.out.println(Arrays.toString(priorityQueueToArray.queue.toArray()));
    }
}
