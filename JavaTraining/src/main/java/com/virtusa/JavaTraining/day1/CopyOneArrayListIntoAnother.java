package com.virtusa.JavaTraining.day1;

import javax.swing.plaf.synth.SynthStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CopyOneArrayListIntoAnother {

    ArrayList<String> names = new ArrayList<String>(Arrays.asList("Prudhvi", "Raju", "Rani"));

    //shallow copy arraylist
    public ArrayList<String> copyList(ArrayList<String> list){
        return (ArrayList<String>) list.clone();
    }

    public static void main(String[] args) {
        CopyOneArrayListIntoAnother obj = new CopyOneArrayListIntoAnother();
        System.out.println(obj.copyList(obj.names));

    }
}
