package com.virtusa.JavaTraining.day2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BufferedProducerConsumer {

    public void singleProducerSingleConsumer(){
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(20);
        Thread producerThread = new Thread(new Producer(queue));
        Thread consumerThread = new Thread(new Consumer(queue));
        producerThread.start();
        consumerThread.start();
    }

    //multiple consumers using executor service
    public void singleProducerMultipleConsumers(int numberOfConsumers){
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(20);
        Thread producerThread = new Thread(new Producer(queue));
        producerThread.start();
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfConsumers);
        for(int i=0;i<numberOfConsumers;i++)
          executorService.submit(new Consumer(queue));
    }

    public static void main(String[] args) {

        BufferedProducerConsumer bufferedProducerConsumer = new BufferedProducerConsumer();
        bufferedProducerConsumer.singleProducerSingleConsumer();
        bufferedProducerConsumer.singleProducerMultipleConsumers(5);


    }

}

class Producer implements Runnable{

    BlockingQueue<Integer> sharedQueue;

    Producer(BlockingQueue<Integer> queue){
        this.sharedQueue = queue;
    }

    @Override
    public void run() {
        for(int i=0;i<100;i++){
            try {
                System.out.println("Started Enqueue:" + i + " "+ Thread.currentThread().getName());
                sharedQueue.put(i);
                System.out.println("Successfully Enqueued:" + i + " "+ Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Error while enqueing:" + e.getMessage());
            }
        }

    }
}

class Consumer implements Runnable{

    BlockingQueue<Integer> sharedQueue;

    Consumer( BlockingQueue<Integer> queue){
        this.sharedQueue = queue;
    }

    @Override
    public void run() {
       while (true){
           try {
               if(!sharedQueue.isEmpty()) {
                   System.out.println("Dequeuing Element:" + sharedQueue.peek() + " "+ Thread.currentThread().getName());
                   int element = sharedQueue.take();
                   System.out.println("Dequeued Element:" + element+ " "+ Thread.currentThread().getName());
                   Thread.sleep(30);
               }
           } catch (InterruptedException e) {
               System.out.println("Error while dequeuing:" + e.getMessage());
           }
       }
    }
}


