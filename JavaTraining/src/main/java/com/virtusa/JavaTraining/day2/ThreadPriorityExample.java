package com.virtusa.JavaTraining.day2;

class ThreadPriority extends Thread{

    ThreadPriority(String threadName){
        super(threadName);
    }

    @Override
    public void run(){
        System.out.println(getName() + " Priority = " + getPriority());
    }

}

public class ThreadPriorityExample {

    public static void main(String[] args) {

        // priority working depends on underlying platform
        Thread t1 = new ThreadPriority("Hyderabad");
        t1.setPriority(Thread.MIN_PRIORITY);
        Thread t2 = new ThreadPriority("Bangalore");
        t2.setPriority(Thread.NORM_PRIORITY);
        Thread t3 = new ThreadPriority("Pune");
        t3.setPriority(Thread.MAX_PRIORITY);

        t1.start();
        t2.start();
        t3.start();
    }



}
