package com.virtusa.JavaTraining.day2;


class ThreadGroupExample implements Runnable {

    ThreadGroupExample(String threadName, ThreadGroup tg) {
        new Thread(tg, this, threadName).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                System.out.println("Exception encounterted");
            }
        }
        System.out.println(Thread.currentThread().getName() +
                " finished executing");
    }
}


public class DisplayThreadsOfThreadGroup {


    public void displayAllThreadsOfThreadGroup(){
        ThreadGroup tg = new ThreadGroup("Cities");
        new ThreadGroupExample("Hyderabad", tg);
        new ThreadGroupExample("Bangalore", tg);
        new ThreadGroupExample("Pune", tg);
        System.out.println("Number of active threads in thread group=" + tg.activeCount());
        Thread[] tarray = new Thread[tg.activeCount()];
        int actualSize = tg.enumerate(tarray);
        for(int i=0;i<actualSize;i++){
            System.out.println("Thread " + tarray[i].getName()+" in thread group " + tg.getName());
        }
    }

    public void displayAllThreadsOfASystem(){
        ThreadGroup tg = Thread.currentThread().getThreadGroup();
        ThreadGroup topGroup = tg;
        while(tg!=null){
            topGroup = tg;
            tg = tg.getParent();
        }
        int estimatedSize = topGroup.activeCount() * 2;
        Thread[] tarray = new Thread[estimatedSize];
        int actualSize = topGroup.enumerate(tarray);
        for(int i=0;i<actualSize;i++){
            System.out.println("Thread " + tarray[i].getName()+" in thread group " + topGroup.getName());
        }

    }



    public static void main(String[] args) {
        DisplayThreadsOfThreadGroup displayThreadsOfThreadGroup = new DisplayThreadsOfThreadGroup();
        displayThreadsOfThreadGroup.displayAllThreadsOfThreadGroup();
        displayThreadsOfThreadGroup.displayAllThreadsOfASystem();
    }

}
