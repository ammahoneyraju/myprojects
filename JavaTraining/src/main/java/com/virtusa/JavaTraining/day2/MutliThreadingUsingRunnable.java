package com.virtusa.JavaTraining.day2;


class MutliThreadingUsingRunnableAndStartInConstructor implements Runnable{

    MutliThreadingUsingRunnableAndStartInConstructor(){
        new Thread(this).start();
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getId() + " is running");
    }
}

public class MutliThreadingUsingRunnable implements Runnable {

    public static void main(String[] args) {
        // passing lambda runnable function
        Thread thread1 = new Thread(() -> {System.out.println("Thread " + Thread.currentThread().getId() + " is running");});
        thread1.start();

        //passing class implementing runnable
        Thread thread2 = new Thread(new MutliThreadingUsingRunnable());
        thread2.start();

        //implementing Runnable Interface having start in constructor
        new MutliThreadingUsingRunnableAndStartInConstructor();

    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getId() + " is running");
    }
}
