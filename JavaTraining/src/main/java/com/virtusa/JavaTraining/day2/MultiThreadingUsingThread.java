package com.virtusa.JavaTraining.day2;

class MultiThreadingUsingThreadAndStartInConstructor extends Thread{

    MultiThreadingUsingThreadAndStartInConstructor(String name){
        super(name);
        start();
    }

    @Override
    public void run(){
        System.out.println("Thread " + Thread.currentThread().getId() + " is running");
    }

}


public class MultiThreadingUsingThread extends Thread{


    @Override
    public void run(){
        System.out.println("Thread " + Thread.currentThread().getId() + " is running");
    }

    public static void main(String[] args) {
        MultiThreadingUsingThread multiThreadingUsingThread = new MultiThreadingUsingThread();
        multiThreadingUsingThread.start();
        MultiThreadingUsingThreadAndStartInConstructor multiThreadingUsingThreadAndStartInConstructor = new MultiThreadingUsingThreadAndStartInConstructor("My Thread");
    }

}
